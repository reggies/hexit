#include "chain.hpp"

#include "../misc.hpp"

namespace hexit
{
    using namespace anim;

    Chain::Chain(int node, int length)
        : mNode(node)
        , mLength(length)
    {
    }
    
    sf::Transform Chain::getTransform() const
    {
        sf::Transform transform;

        const int period = 500;
        const std::chrono::milliseconds phase(period * mNode / mLength / 2);
        const float arg = normalizeTime(mElapsed + phase, period);
        const float scale = 1.f + (1.f - arg) / 6;
        transform.scale(scale, scale);

        return transform;
    }
}
