#include "flip.hpp"

#include "../misc.hpp"

namespace hexit
{
    using namespace anim;

    sf::Transform Flip::getTransform() const
    {
        sf::Transform transform;

        const int angle = 60;
        const int duration = 500;
        
        if(mElapsed < std::chrono::milliseconds(duration)) {
            const float arg = normalizeTime(mElapsed, duration);
            const float scale = fabs(2.f * arg - 1.f);
            transform.rotate(angle);
            transform.scale(scale, 1.f);
            transform.rotate(-angle);
        }
        
        return transform;
    }
}
