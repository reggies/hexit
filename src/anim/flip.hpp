#ifndef FLIP_HPP_
#define FLIP_HPP_

#include "animation.hpp"

namespace hexit
{
    namespace anim
    {
        class Flip : public Animation
        {
        public:
            ~Flip() override {}

            sf::Transform getTransform() const override;
        };
    }
}

#endif
