#include "shake.hpp"

#include "../misc.hpp"

namespace hexit
{
    using namespace anim;

    Shake::Shake()
        : mAngle(rand())
    {
    }

    sf::Transform Shake::getTransform() const
    {
        sf::Transform transform;

        const float maxAmplitude = 15.f;
        const int shakes = 4;
        const int duration = 500;
        const int period = duration / shakes;

        if(mElapsed < std::chrono::milliseconds(duration)) {
            const float arg = normalizeTime(mElapsed, period);
            const float amplitude = maxAmplitude * (2.f * arg - 1.0) * sin(M_PI * arg);
            transform.translate(amplitude * cos(mAngle), amplitude * sin(mAngle));
        }

        return transform;
    }
}
