#ifndef ANIMATION_H_
#define ANIMATION_H_

#include <chrono>

#include <SFML/Graphics/Transform.hpp>

namespace hexit
{
    namespace anim
    {
        /**
         * \brief Cell's transformation state.
         *
         * It models "motion" effects through sf::Transform modifications.
         */
        class Animation
        {    
        public:
            Animation();
            virtual ~Animation() {}
        
            virtual sf::Transform getTransform() const = 0;
            virtual void update(const std::chrono::milliseconds &delta);
        
        protected:
            std::chrono::milliseconds mElapsed;
        };
    }
}

#endif // ANIMATION_H_
