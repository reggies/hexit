#include "animation.hpp"

namespace hexit
{
    using namespace anim;

    Animation::Animation()
        : mElapsed(0)
    {
    }
    
    void Animation::update(const std::chrono::milliseconds &delta)
    {
        mElapsed += delta;
    }
}
