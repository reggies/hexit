#ifndef SHAKE_HPP_
#define SHAKE_HPP_

#include "animation.hpp"

namespace hexit
{
    namespace anim
    {
        class Shake : public Animation
        {
        public:
            Shake();
            ~Shake() override {}

            sf::Transform getTransform() const override;

        private:
            float mAngle;
        };
    }
}

#endif
