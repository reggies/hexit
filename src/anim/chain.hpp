#ifndef CHAIN_HPP_
#define CHAIN_HPP_

#include "animation.hpp"

namespace hexit
{
    namespace anim
    {
        class Chain : public Animation
        {
        public:
            Chain(int node, int length);
            ~Chain() override {}
            sf::Transform getTransform() const override;

        private:
            int mNode;
            int mLength;
        };
    }
}

#endif
