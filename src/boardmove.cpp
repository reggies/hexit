#include "boardmove.hpp"

#include <stdexcept>

#include "board.hpp"

namespace hexit
{
    BoardMove::BoardMove(MoveType type)
        : mType(type)
    {
        if(type == MoveType::Cell) {
            throw std::logic_error("bad type");
        }
    }

    BoardMove::BoardMove(MoveType type, const CellId &id)
        : mType(type)
        , mTargetCell(id)
    {
        if(type != MoveType::Cell) {
            throw std::logic_error("bad type");
        }
    }

    MoveType BoardMove::type() const
    {
        return mType;
    }

    CellId BoardMove::id() const
    {
        if(mType != MoveType::Cell) {
            throw std::logic_error("bad type");
        }
        return mTargetCell;
    }

    bool BoardMove::canApplyTo(const Board &board) const
    {
        switch(mType) {
        case MoveType::Null: return false;
        case MoveType::Pie: return board.isPieAvailable();
        case MoveType::Cell: return board.hasCell(mTargetCell) && board.getCell(mTargetCell).empty();
        default: return false;
        }
    }

    void BoardMove::applyTo(Board *board) const
    {
        if(!canApplyTo(*board)) {
            return;
        }

        switch(mType) {
        case MoveType::Null:
            break;

        case MoveType::Pie:
            {
                for(const CellId &id : board->getCells()) {
                    const CellValue &value = board->getCell(id);
                    if(!value.empty()) {
                        board->setCell(id,
                            CellValue(
                                board->getOtherPlayerId(
                                    value.player())));
                    }
                }
                board->beginNextTurn();
            }
            break;

        case MoveType::Cell:
            {
                const unsigned player = board->getCurrentPlayerId();
                board->setCell(mTargetCell, CellValue(player));
                board->beginNextTurn();
            }
            break;

        default:
            break;
        }
    }
}
