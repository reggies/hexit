#pragma once

#include <chrono>
#include <vector>
#include <memory>
#include <stack>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>

#include "board.hpp"
#include "boardview.hpp"

#include "states/istate.hpp"
#include "states/context.hpp"

namespace hexit
{
    class Game
    {
    public:
        Game();
        void run();

    private:
        sf::RenderWindow mWindow;
        std::stack<std::unique_ptr<states::IState>> mStates;
        states::Context mContext;
        std::chrono::steady_clock::time_point mLastUpdate;

        void handleTransition(states::Transition transition);
        void processEvents();
        void update();
        void draw();
    };
}
