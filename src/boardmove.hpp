#ifndef BOARDMOVE_H_
#define BOARDMOVE_H_

#include "cellid.hpp"

namespace hexit
{
    class Board;
}

namespace hexit
{
    enum class MoveType {Null, Pie, Cell};

    /**
     * \brief Decision's command.
     */
    class BoardMove
    {
    public:
        BoardMove(MoveType type = MoveType::Null);
        BoardMove(MoveType type, const CellId &id);

        BoardMove(const BoardMove& that) = default;
        BoardMove& operator=(const BoardMove& that) = default;

        BoardMove(BoardMove&& that) = default;
        BoardMove& operator=(BoardMove&& that) = default;

        MoveType type() const;
        CellId id() const;

        bool canApplyTo(const Board &board) const;
        void applyTo(Board *board) const;

    private:
        MoveType mType;
        CellId mTargetCell;
    };
}

#endif // BOARDMOVE_H_
