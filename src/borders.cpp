#include "borders.hpp"

#include "misc.hpp"
#include "geometry.hpp"
#include "board.hpp"
#include "boardgrid.hpp"

sf::ConvexShape getShapeFromPoints(const std::vector<sf::Vector2f> &points)
{
    sf::ConvexShape shape(points.size());
    for(size_t i = 0; i < points.size(); ++i) {
        shape.setPoint(i, points.at(i));
    }
    return shape;
}

namespace hexit
{
    Borders::Borders(const Board &board, const BoardGrid &grid, float cellRadius, float width, const sf::Color &bg, const std::vector<sf::Color> &palette)
        : mBoard(board)
        , mGrid(grid)
        , mCellRadius(cellRadius)
        , mBorderWidth(width)
        , mColors(palette)
    {
        const std::vector<sf::Vector2f> &corners = mGrid.getCorners(mBoard, mCellRadius);
        const sf::Vector2f &center = getRectCenter(getBoundingRect(corners));

        const std::vector<BoardSide> &sides = {
            BoardSide::Top,
            BoardSide::Right,
            BoardSide::Bottom,
            BoardSide::Left
        };

        size_t iCorner = 0;
        for(const BoardSide &side : sides) {
            const std::vector<sf::Vector2f> &points = {
                center,
                corners.at(modulo(iCorner, corners.size())),
                corners.at(modulo(iCorner + 1, corners.size()))
            };
            iCorner += 1;
            
            sf::ConvexShape border = getShapeFromPoints(points);
            border.setFillColor(mColors.at(mBoard.getSidePlayer(side)));
            border.setOrigin(center);
            border.setPosition(center);
            mBorders.emplace_back(std::move(border));
        }

        const float scaleFactor = mBorderWidth / mCellRadius;
        const float scale = (mBoard.size() - scaleFactor) / mBoard.size();
        
        sf::ConvexShape background = getShapeFromPoints(corners);
        background.setFillColor(bg);
        background.setOrigin(center);
        background.setScale(scale, scale);
        background.setPosition(center);
        mBorders.emplace_back(std::move(background));
    }

    sf::FloatRect Borders::getBounds() const
    {
        std::vector<sf::FloatRect> bounds;
        for(const sf::Shape &shape : mBorders) {
            bounds.push_back(shape.getLocalBounds());
        }
        return getCombinedRect(bounds);
    }

    void Borders::draw(sf::RenderTarget &target, sf::RenderStates states) const
    {
        for(const sf::Drawable &border : mBorders) {
            target.draw(border, states);
        }
    }
}
