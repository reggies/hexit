#include "boardview.hpp"

#include <cmath>

#include "board.hpp"
#include "colorutils.hpp"
#include "misc.hpp"
#include "cellvalue.hpp"
#include "cellstate.hpp"
#include "geometry.hpp"

#include "anim/chain.hpp"
#include "anim/flip.hpp"
#include "anim/shake.hpp"

namespace hexit
{
    BoardView::BoardView(const Board &board, float cellRadius, const std::vector<sf::Color> &palette)
        : mBoard(board)
        , mShowSelection(false)
        , mColors(palette)
        , mEmptyCellColor(getDarkenColor(foldBlendColors(mColors)))
        , mCellOutlineColor(getLightenColor(foldBlendColors(mColors)))
        , mDrawOrder(0)
        , mCellRadius(cellRadius)
        , mGrid()
        , mBorders(mBoard, mGrid, cellRadius, cellRadius / 2, mEmptyCellColor, mColors)
    {
        for(const CellId &id : mBoard.getCells()) {
            const sf::Color &color = getCellColor(id);
            Cell cell(id);
            cell.setState<BoardCellState>(mCellRadius, color, mCellOutlineColor);
            cell.setPosition(mGrid.getCellPosition(id, mCellRadius));
            mCells.emplace(id, std::move(cell));
        }
    }

    void BoardView::activateSelection(bool activated)
    {
        mShowSelection = activated;
    }

    CellId BoardView::getSelectedCell() const
    {
        return mSelection.getId();
    }

    void BoardView::setSelectedCell(const CellId &id)
    {
        if(mBoard.hasCell(id)) {
            mSelection.setId(id);

            const sf::Color &color = getLightenColor(getPlayerColor(mBoard.getCurrentPlayerId()));
            mSelection.setState<SelectionCellState>(mCellRadius, color);

            const Cell &targetCell = mCells.at(id);
            mSelection.setPosition(targetCell.getPosition());
        }
    }

    void BoardView::update(const std::chrono::milliseconds &delta)
    {
        mSelection.update(delta);
        for(auto &kv : mCells) {
            kv.second.update(delta);
        }
    }

    sf::FloatRect BoardView::getLocalBounds() const
    {
        return mBorders.getBounds();
    }

    sf::FloatRect BoardView::getGlobalBounds() const
    {
        return getTransform().transformRect(getLocalBounds());
    }

    sf::Color BoardView::getPlayerColor(unsigned player) const
    {
        return mColors.at(player);
    }

    sf::Color BoardView::getCellValueColor(const CellValue &value) const
    {
        if(value.empty()) {
            return mEmptyCellColor;
        } else {
            return getPlayerColor(value.player());
        }
    }

    sf::Color BoardView::getCellColor(const CellId &id) const
    {
        return getCellValueColor(mBoard.getCell(id));
    }

    void BoardView::updateCellValue(const CellId &id, const CellValue &value)
    {
        Cell &cell = mCells[id];
        cell.setAnimation<anim::Flip>();

        const sf::Color &newColor = getCellValueColor(value);
        const sf::Color &oldColor = getCellColor(id);
        cell.setState<TransitionCellState>(mCellRadius, oldColor, newColor, mCellOutlineColor);

        bringToFront(&cell);
    }

    void BoardView::shakeSelection()
    {
        mSelection.setAnimation<anim::Shake>();
        bringToFront(&mSelection);
    }

    void BoardView::animateChain(const std::vector<CellId> &chain)
    {
        const int length = chain.size();
        int nodeNum = 0;

        for(const CellId &id : chain) {
            Cell &cell = mCells[id];
            cell.setAnimation<anim::Chain>(nodeNum, length);
            bringToFront(&cell);
            ++nodeNum;
        }
    }

    void BoardView::moveSelection(int dx, int dy)
    {
        const unsigned int boardSize = mBoard.size();
        const CellId &id = mSelection.getId();
        setSelectedCell(
            CellId(modulo(id.x + dx + boardSize, boardSize),
                   modulo(id.y + dy + boardSize, boardSize)));
    }

    void BoardView::bringToFront(Cell *cell)
    {
        ++mDrawOrder;
        cell->setDrawOrder(mDrawOrder);
    }

    std::vector<CellId> BoardView::getOrderedCells() const
    {
        std::vector<CellId> cells = mBoard.getCells();

        std::stable_sort(cells.begin(), cells.end(),
                         [this](const CellId &lhs, const CellId &rhs) {
                             const Cell &lcell = mCells.at(lhs);
                             const Cell &rcell = mCells.at(rhs);
                             return lcell.getDrawOrder() < rcell.getDrawOrder();
                         });

        return cells;
    }

    void BoardView::draw(sf::RenderTarget &target, sf::RenderStates states) const
    {
        states.transform *= getTransform();

        target.draw(mBorders, states);

        for(const CellId &id : getOrderedCells()) {
            target.draw(mCells.at(id), states);
        }

        if(mShowSelection) {
            target.draw(mSelection, states);
        }
    }
}
