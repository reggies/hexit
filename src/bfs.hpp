#ifndef BFS_H_
#define BFS_H_

#include <vector>

#include "cellmap.hpp"
#include "cellid.hpp"

namespace hexit
{
    class Board;
}

namespace hexit
{
    /**
     * \brief Helper class for component search.
     */
    class BreadthFirstTree
    {
    public:
        explicit BreadthFirstTree(const Board &board);

        void addVertex(const CellId &cell);
        bool wasDiscovered(const CellId &cell) const;
        std::vector<CellId> getPath(const CellId &cell) const;
        
    private:
        const Board &mBoard;
        CellMap<bool> mVisited;
        CellMap<CellId> mPredecessor;

    private:
        bool discover(const CellId &source, const CellId &target);
    };
}

#endif // BFS_H_
