#include "cellstate.hpp"

#include "colorutils.hpp"
#include "misc.hpp"
#include "hexagonshape.hpp"

const float kCellOutlineThickness = 1.f;
const float kSelectionOutlineThickness = 5.f * kCellOutlineThickness;

namespace hexit
{
    CellState::CellState()
        : mElapsed(0)
    {
    }
    
    void CellState::update(const std::chrono::milliseconds &elapsed)
    {
        mElapsed += elapsed;
    }
    
    std::chrono::milliseconds CellState::getElapsedTime() const
    {
        return mElapsed;
    }
}

namespace hexit
{
    SelectionCellState::SelectionCellState(float cellRadius, const sf::Color &color)
        : mCellRadius(cellRadius)
        , mColor(color)
    {
    }
    
    void SelectionCellState::draw(sf::RenderTarget &target, sf::RenderStates states) const
    {
        HexagonShape shape(mCellRadius);
        
        shape.setOutlineThickness(kSelectionOutlineThickness);
        shape.setOutlineColor(mColor);
        shape.setFillColor(sf::Color::Transparent);
        shape.setOrigin(shape.getRadius(), shape.getRadius());

        target.draw(shape, states);
    }
}

namespace hexit
{
    BoardCellState::BoardCellState(float cellRadius, const sf::Color &fill, const sf::Color &outline)
        : mCellRadius(cellRadius)
        , mFillColor(fill)
        , mOutlineColor(outline)
    {
    }
    
    void BoardCellState::draw(sf::RenderTarget &target, sf::RenderStates states) const
    {
        HexagonShape shape(mCellRadius);

        shape.setOutlineThickness(kCellOutlineThickness);
        shape.setOutlineColor(mOutlineColor);
        shape.setFillColor(mFillColor);
        shape.setOrigin(shape.getRadius(), shape.getRadius());

        target.draw(shape, states);
    }
}

namespace hexit
{
    TransitionCellState::TransitionCellState(float cellRadius, const sf::Color &init, const sf::Color &last, const sf::Color &outline)
        : mCellRadius(cellRadius)
        , mInitColor(init)
        , mLastColor(last)
        , mOutlineColor(outline)
    {
    }
    
    void TransitionCellState::draw(sf::RenderTarget &target, sf::RenderStates states) const
    {
        HexagonShape shape(mCellRadius);

        shape.setOutlineThickness(kCellOutlineThickness);
        shape.setOutlineColor(mOutlineColor);
        shape.setOrigin(shape.getRadius(), shape.getRadius());

        const int duration = 500;
        
        if(getElapsedTime() < std::chrono::milliseconds(duration)) {
            const float done = normalizeTime(getElapsedTime(), duration);
            shape.setFillColor(mixColors(mInitColor, mLastColor, done));
        } else {
            shape.setFillColor(mLastColor);
        }

        target.draw(shape, states);
    }
}
