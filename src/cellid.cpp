#include "cellid.hpp"

#include <iostream>

namespace hexit
{
    CellId::CellId()
        : x(0), y(0)
    {
    }
    
    CellId::CellId(int xCoord, int yCoord)
        : x(xCoord), y(yCoord)
    {
    }
    
    bool CellId::operator==(const CellId &that) const
    {
        return (x == that.x) && (y == that.y);
    }
    
    bool CellId::operator!=(const CellId &that) const
    {
        return !this->operator==(that);
    }

    std::ostream& operator<<(std::ostream &out, const CellId &cell)
    {
        out << '[' << cell.x << ',' << cell.y << ']';
        return out;
    }
}
