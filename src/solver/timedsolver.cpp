#include "timedsolver.hpp"

#include <algorithm>
#include <chrono>

#include "utils.hpp"

namespace hexit
{
    using namespace solver;

    TimedSolver::TimedSolver(const std::chrono::milliseconds& timeout)
        : mTimeout(timeout)
    {}

    solver::Result TimedSolver::solve(const Board &board)
    {
        const auto& weights = calculateCellWeights(board, mTimeout);

        if(!weights)
            return weights.Err();

        const std::vector<CellId> &cells =
            getSortedDescending(board.getCells(), *weights);

        const bool usePie = board.isPieAvailable();
        const auto &bestResult = getBestCell(board, cells, usePie);

        if(bestResult) {
            if(usePie && !board.getCell(*bestResult).empty()) {
                return BoardMove(MoveType::Pie);
            } else {
                return BoardMove(MoveType::Cell, *bestResult);
            }
        } else {
            return bestResult.Err();
        }
    }
}
