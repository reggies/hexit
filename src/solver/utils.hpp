#pragma once

#include "../board.hpp"
#include "../boardmove.hpp"

#include "../cellmap.hpp"

namespace hexit
{
    namespace solver
    {
        namespace
        {
            hexit::Result<Board, std::string>
            applyMoves(const Board &board, const std::vector<BoardMove> &moves)
            {
                Board temp(board);
                for(const BoardMove &move : moves) {
                    if(!move.canApplyTo(temp)) {
                        return {"bad board"};
                    }
                    move.applyTo(&temp);
                }
                return temp;
            }

            hexit::Result<Board, std::string>
            applyCells(const Board &board, const std::vector<CellId> &cells)
            {
                std::vector<BoardMove> moves(cells.size());
                for(size_t i = 0; i < cells.size(); ++i) {
                    moves[i] = BoardMove(MoveType::Cell, cells[i]);
                }
                return applyMoves(board, moves);
            }

            void updateCellWeights(const Board &board, CellMap<unsigned> *weights)
            {
                unsigned winner;
                if(board.getWinnerPlayerId(&winner)) {
                    for(const CellId &cell : board.getCells()) {
                        const CellValue &value = board.getCell(cell);
                        if(value == CellValue(winner)) {
                            ++weights->operator[](cell);
                        }
                    }
                }
            }

            hexit::Result<CellMap<unsigned>, std::string>
            calculateCellWeights(const Board &board, const std::chrono::milliseconds &timeout)
            {
                using namespace std::chrono;
                const steady_clock::time_point endTime = steady_clock::now() + timeout;

                std::vector<CellId> cells = board.collectEmptyCells();

                CellMap<unsigned> weights;
                while(steady_clock::now() < endTime) {
                    std::random_shuffle(cells.begin(), cells.end());
                    const auto &temp = applyCells(board, cells);
                    if (temp) {
                        updateCellWeights(*temp, &weights);
                    } else {
                        return temp.Err();
                    }
                }

                return weights;
            }

            std::vector<CellId> getSortedDescending(
                const std::vector<CellId> &cells,
                const CellMap<unsigned> &weights)
            {
                std::vector<CellId> sorted = cells;
                std::sort(sorted.begin(), sorted.end(),
                          [&weights](const CellId &lhs, const CellId &rhs) {
                              return weights.at(lhs) > weights.at(rhs);
                          });
                return sorted;
            }

            hexit::Result<CellId, std::string>
            getBestCell(const Board &board, const std::vector<CellId> &cells, bool usePie)
            {
                for(CellId id : cells) {
                    const bool isEmpty = board.getCell(id).empty();
                    if(usePie || isEmpty) {
                        return id;
                    }
                }
                return std::string("bad board");
            }
        }
    }
}
