#pragma once

#include <chrono>

#include "isolver.hpp"

namespace hexit
{
    namespace solver
    {
        class TimedSolver : public ISolver {
        public:
            explicit TimedSolver(const std::chrono::milliseconds& timeout);
            ~TimedSolver() override {}

            Result solve(const Board &board) override;

        private:
            std::chrono::milliseconds mTimeout;
        };
    }
}
