#pragma once

#include <string>

#include "../board.hpp"

#include "result.hpp"

namespace hexit
{
    namespace solver
    {
        class ISolver {
        public:
            virtual ~ISolver() {}
            virtual Result solve(const Board& board) = 0;
        };
    }
}
