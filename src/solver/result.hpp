#pragma once

#include "../result.hpp"
#include "../boardmove.hpp"

namespace hexit
{
    namespace solver
    {
        using Result = ::hexit::Result<BoardMove, std::string>;
    }
}
