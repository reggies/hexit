#pragma once

#include "isolver.hpp"

namespace hexit
{
    namespace solver
    {
        class RandomSolver : public ISolver
        {
        public:
            ~RandomSolver() override {}
            solver::Result solve(const Board& board) override;
        };
    }
}
