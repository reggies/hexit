#include "randomsolver.hpp"

namespace hexit
{
    using namespace solver;

    solver::Result RandomSolver::solve(const Board& board)
    {
        const auto& moves = board.collectValidMoves();

        if(!moves.empty()) {
            BoardMove move = moves.at(rand() % moves.size());
            return move;
        } else{
            return solver::Result("no moves available");
        }
    }
}
