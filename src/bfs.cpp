#include "bfs.hpp"

#include <queue>

#include "board.hpp"

namespace hexit
{    
    BreadthFirstTree::BreadthFirstTree(const Board &board)
        : mBoard(board)
    {
    }

    std::vector<CellId> getAdjacentCells(const CellId &cell)
    {
        return {
            {cell.x,     cell.y + 1},
            {cell.x,     cell.y - 1},
            {cell.x - 1, cell.y + 1},
            {cell.x - 1, cell.y},
            {cell.x + 1, cell.y - 1},
            {cell.x + 1, cell.y}
        };
    }
    
    void BreadthFirstTree::addVertex(const CellId &source)
    {
        std::queue<CellId> Q;
        Q.push(source);
        mVisited[source] = true;
        mPredecessor[source] = source;

        while(!Q.empty()) {
            const CellId &head = Q.front();
            Q.pop();
            for(const CellId &tail : getAdjacentCells(head)) {
                if(discover(head, tail)) {
                    Q.push(tail);
                }
            }
        }        
    }

    bool BreadthFirstTree::discover(const CellId &source, const CellId &target)
    {
        if(!mBoard.hasCell(target)) {
            return false;
        }

        if(mBoard.getCell(target) != mBoard.getCell(source)) {
            return false;
        }

        bool &isVisited = mVisited[target];
        if(isVisited) {
            return false;
        }

        isVisited = true;
        mPredecessor[target] = source;
        return true;
    }
    
    bool BreadthFirstTree::wasDiscovered(const CellId &cell) const
    {
        return mVisited.find(cell) != mVisited.end();
    }

    std::vector<CellId> BreadthFirstTree::getPath(const CellId &end) const
    {
        std::vector<CellId> chain;
        
        CellId node = end;
        chain.push_back(node);
        while(getOrElse(mPredecessor, node, node) != node) {
            node = getOrElse(mPredecessor, node, node);
            chain.push_back(node);
        }

        return chain;
    }

}
