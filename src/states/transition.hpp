#pragma once

#include <memory>

namespace hexit
{
    namespace states
    {
        class IState;

        enum class TransitionType {
            None,
            Pop,
            Push,
            Replace,
            Quit
        };

        class Transition {
        public:
            Transition() = delete;

            static Transition none();
            static Transition pop();
            static Transition push(std::unique_ptr<IState> state);
            static Transition replace(std::unique_ptr<IState> state);
            static Transition quit();

            TransitionType type() const;
            std::unique_ptr<IState> grabState();

        private:
            explicit Transition(TransitionType type,
                                std::unique_ptr<IState> state = nullptr);

            TransitionType mType;
            std::unique_ptr<IState> mState;
        };
    }
}
