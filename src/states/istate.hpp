#pragma once

#include "context.hpp"
#include "transition.hpp"

#include <SFML/Window/Event.hpp>

namespace hexit
{
    namespace states
    {
        class IState
        {
        public:
            virtual ~IState() {}

            virtual Transition started() {
                return Transition::none();
            }

            virtual Transition finished() {
                return Transition::none();
            }

            virtual Transition hidden() {
                return Transition::none();
            }

            virtual Transition exposed() {
                return Transition::none();
            }

            virtual Transition update(const std::chrono::milliseconds &delta) = 0;
            virtual Transition handleEvent(const sf::Event &event) = 0;
        };
    }
}
