#pragma once

#include "istate.hpp"

namespace hexit
{
    namespace states
    {
        class GameOver : public IState
        {
        public:
            GameOver(Context* ctx, const std::vector<CellId> &chain);
            ~GameOver() override {}

            Transition started() override;
            Transition finished() override;

            Transition update(const std::chrono::milliseconds &delta) override;
            Transition handleEvent(const sf::Event &event) override;

        private:
            Context* mCtx;
        };
    }
}
