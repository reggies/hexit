#include "gameplay.hpp"

#include <iostream>

#include "gameover.hpp"

#include "../misc.hpp"
#include "../solver/timedsolver.hpp"
#include "../solver/randomsolver.hpp"

namespace hexit
{
    const float kCellRadius = 20.f;

    namespace
    {
        template<class T>
        bool isFutureReady(const std::future<T> &fut)
        {
            return fut.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
        }
    }
}

namespace hexit
{
    using namespace states;

    GamePlay::GamePlay(Context* ctx, int boardSize)
        : mCtx(ctx)
    {
        mCtx->board.reset(new Board(boardSize));
        mCtx->boardView.reset(new BoardView(*mCtx->board, kCellRadius, mCtx->colorSet));
        mCtx->boardView->setSelectedCell(CellId(boardSize / 2, boardSize / 2));

        mCtx->boardView->setOrigin(
            getRectCenter(mCtx->boardView->getLocalBounds()));

        nextTurnState();
    }

    GamePlay::~GamePlay()
    {
        if(mSearchResult.valid()) {
            mSearchResult.wait();
        }
    }

    void GamePlay::nextTurnState()
    {
        auto currentPlayerId = mCtx->board->getCurrentPlayerId();
        const PlayerType &currentPlayer = mCtx->players.at(currentPlayerId);

        if(currentPlayer == PlayerType::Human) {
            mCtx->boardView->activateSelection(true);
        } else {
            Board board = *mCtx->board;
            auto timeQuota = mCtx->robotThinkingTime;
            auto finder = [board, timeQuota](std::promise<solver::Result> result) {
                solver::TimedSolver solver(timeQuota);
                result.set_value(solver.solve(board));
            };

            std::promise<solver::Result> provider;
            mSearchResult = provider.get_future();
            std::thread worker(finder, std::move(provider));
            worker.detach();

            mCtx->boardView->activateSelection(false);
        }
    }

    Transition GamePlay::update(const std::chrono::milliseconds &delta)
    {
        mCtx->boardView->update(delta);

        auto currentPlayerId = mCtx->board->getCurrentPlayerId();

        if(mCtx->players.at(currentPlayerId) == PlayerType::Bot) {
            if(isFutureReady(mSearchResult)) {
                const solver::Result &result = mSearchResult.get();
                if(result) {
                    pushMove(*result);
                } else {
                    solver::RandomSolver solver;
                    const solver::Result& result =
                        solver.solve(*mCtx->board);
                    if(result) {
                        pushMove(*result);
                    }
                }
            }
        }

        BoardMove move;
        if(pollMove(&move)) {
            visualizeMove(move);
            move.applyTo(mCtx->board.get());

            const std::vector<CellId> &chain
                = mCtx->board->findCompleteChain(currentPlayerId);
            if(!chain.empty()) {
                return Transition::push(
                    std::unique_ptr<states::IState>(new GameOver(mCtx, chain)));
            } else {
                nextTurnState();
            }
        }
        return Transition::none();
    }

    void GamePlay::selectUp()
    {
        mCtx->boardView->moveSelection(0, -1);
    }

    void GamePlay::selectDown()
    {
        mCtx->boardView->moveSelection(0, 1);
    }

    void GamePlay::selectLeft()
    {
        mCtx->boardView->moveSelection(-1, 0);
    }

    void GamePlay::selectRight()
    {
        mCtx->boardView->moveSelection(1, 0);
    }

    bool GamePlay::pollMove(BoardMove *move)
    {
        if(!mMaybeMove.empty()) {
            *move = mMaybeMove.at(0);
            mMaybeMove.clear();
            return true;
        }
        return false;
    }

    void GamePlay::pushMove(const BoardMove &move)
    {
        mMaybeMove = {move};
    }

    void GamePlay::chooseCell()
    {
        const BoardMove move(MoveType::Cell,
                             mCtx->boardView->getSelectedCell());
        pushMove(move);
    }

    void GamePlay::choosePie()
    {
        pushMove(BoardMove(MoveType::Pie));
    }

    Transition GamePlay::handleEvent(const sf::Event &event)
    {
        // BUG we must disable user input handling in the
        // non-human controllable turns
        if(event.type == sf::Event::KeyPressed) {
            switch(event.key.code) {
            case sf::Keyboard::Left:
                selectLeft();
                break;

            case sf::Keyboard::Right:
                selectRight();
                break;

            case sf::Keyboard::Up:
                selectUp();
                break;

            case sf::Keyboard::Down:
                selectDown();
                break;

            case sf::Keyboard::Return:
                chooseCell();
                break;

            case sf::Keyboard::Space:
                choosePie();
                break;

            default:
                break;
            }
        }
        return Transition::none();
    }

    void GamePlay::visualizeMove(const BoardMove &move)
    {
        Board &board = *mCtx->board;
        BoardView &view = *mCtx->boardView;

        if(move.type() == MoveType::Pie) {
            if(move.canApplyTo(board)) {
                for(const CellId &cell : board.getCells()) {
                    const CellValue &value = board.getCell(cell);
                    if(!value.empty()) {
                        unsigned other = board.getOtherPlayerId(value.player());
                        view.updateCellValue(cell, CellValue(other));
                    }
                }
            } else {
                /// \todo notify user that the pie is over
            }
        } else if(move.type() == MoveType::Cell) {
            if(move.canApplyTo(board)) {
                view.updateCellValue(move.id(), CellValue(board.getCurrentPlayerId()));
            } else {
                view.shakeSelection();
            }
        }
    }
}
