#include "gamestate.hpp"

namespace hexit
{
    GameState::GameState(Game* game)
        : mGame(game)
        , mContext(&game->mContext) {}
}
