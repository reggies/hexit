#include "transition.hpp"

#include "istate.hpp"

namespace hexit
{
    using namespace states;

    Transition::Transition(TransitionType type, std::unique_ptr<IState> state)
        : mType(type), mState(std::move(state))
    {}

    Transition Transition::none()
    {
        return Transition(TransitionType::None);
    }

    Transition Transition::pop()
    {
        return Transition(TransitionType::Pop);
    }

    Transition Transition::push(std::unique_ptr<IState> state)
    {
        return Transition(TransitionType::Push, std::move(state));
    }

    Transition Transition::replace(std::unique_ptr<IState> state)
    {
        return Transition(TransitionType::Replace, std::move(state));
    }

    Transition Transition::quit()
    {
        return Transition(TransitionType::Quit);
    }

    TransitionType Transition::type() const
    {
        return mType;
    }

    std::unique_ptr<IState> Transition::grabState()
    {
        return std::move(mState);
    }
}
