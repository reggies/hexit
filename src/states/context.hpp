#pragma once

#include <vector>
#include <memory>
#include <chrono>

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>

#include "../board.hpp"
#include "../boardview.hpp"
#include "../playertype.hpp"

namespace hexit
{
    namespace states
    {
        struct Context
        {
            sf::Font font;
            std::chrono::milliseconds robotThinkingTime;
            std::unique_ptr<Board> board;
            std::unique_ptr<BoardView> boardView;
            std::vector<PlayerType> players;
            std::vector<sf::Color> colorSet;
            sf::Text message;
        };
    }
}
