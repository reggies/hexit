#pragma once

#include <map>
#include <functional>
#include <memory>

#include <future>

#include <SFML/Window/Keyboard.hpp>

#include "istate.hpp"
#include "../boardmove.hpp"
#include "../solver/result.hpp"

namespace hexit
{
    namespace states
    {
        class GamePlay : public IState
        {
        public:
            GamePlay(Context* ctx, int boardSize);
            ~GamePlay() override;

            Transition update(const std::chrono::milliseconds &delta) override;
            Transition handleEvent(const sf::Event &event) override;

        private:
            void visualizeMove(const BoardMove &move);

            void selectUp();
            void selectDown();
            void selectLeft();
            void selectRight();
            void chooseCell();
            void choosePie();
            void nextTurnState();

            bool pollMove(BoardMove *move);
            void pushMove(const BoardMove& move);

        private:
            Context* mCtx;
            std::vector<BoardMove> mMaybeMove;
            std::future<solver::Result> mSearchResult;
        };
    }
}
