#include "gameover.hpp"

#include "../misc.hpp"
#include "istate.hpp"
#include "gameplay.hpp"

namespace hexit
{
    using namespace states;

    GameOver::GameOver(Context* ctx, const std::vector<CellId> &chain)
        : mCtx(ctx)
    {
        mCtx->boardView->activateSelection(false);
        mCtx->boardView->animateChain(chain);
    }

    Transition GameOver::started()
    {
        mCtx->message.setString("Game over");
        mCtx->message.setOrigin(
            getRectCenter(mCtx->message.getLocalBounds()));
        return Transition::none();
    }

    Transition GameOver::finished()
    {
        mCtx->message.setString("");
        return Transition::none();
    }

    Transition GameOver::update(const std::chrono::milliseconds &delta)
    {
        mCtx->boardView->update(delta);
        return Transition::none();
    }

    Transition GameOver::handleEvent(const sf::Event &event)
    {
        if(event.type == sf::Event::KeyPressed) {
            if(event.key.code == sf::Keyboard::Return) {
                return Transition::replace(
                    std::unique_ptr<GamePlay>(
                        new GamePlay(mCtx, mCtx->board->size())));
            }
        }
        return Transition::none();
    }
}
