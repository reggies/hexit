#ifndef BOARDGRID_H_
#define BOARDGRID_H_

#include <vector>

#include "board.hpp"
#include "cellid.hpp"

#include <SFML/System/Vector2.hpp>

namespace hexit
{
    /**
     * \brief Cells' placement strategy.
     */
    class BoardGrid
    {
    public:
        sf::Vector2f getCellPosition(const CellId &cell, float radius) const;
        std::vector<sf::Vector2f> getCorners(const Board &board, float radius) const;
    };
}

#endif // BOARDGRID_H_
