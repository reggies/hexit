#ifndef HEXAGONSHAPE_H_
#define HEXAGONSHAPE_H_

#include <SFML/Graphics/CircleShape.hpp>

namespace hexit
{
    class HexagonShape : public sf::CircleShape
    {
    public:
        HexagonShape(float radius);
        virtual ~HexagonShape() = default;

        using sf::CircleShape::setRadius;
        using sf::CircleShape::getRadius;
    };
}

#endif // HEXAGONSHAPE_H_
