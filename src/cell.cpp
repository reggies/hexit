#include "cell.hpp"

#include "hexagonshape.hpp"

namespace hexit
{
    Cell::Cell(const CellId &id)
        : mCell(id)
        , mDrawOrder(0)
    {
    }

    CellId Cell::getId() const
    {
        return mCell;
    }

    void Cell::setId(const CellId &cell) 
    {
        mCell = cell;
    }

    void Cell::setDrawOrder(int order)
    {
        mDrawOrder = order;
    }

    int Cell::getDrawOrder() const
    {
        return mDrawOrder;
    }
    
    void Cell::draw(sf::RenderTarget &target, sf::RenderStates states) const
    {
        states.transform *= getTransform();
        const sf::Transform &motion = (mAnimation ? mAnimation->getTransform() : sf::Transform::Identity);

        if(mState) {
            states.transform *= motion;
            mState->draw(target, states);
        }
    }
    
    void Cell::update(const std::chrono::milliseconds &elapsed)
    {
        if(mAnimation) {
            mAnimation->update(elapsed);
        }

        if(mState) {
            mState->update(elapsed);
        }
    }
}
