#pragma once

#include <string>

namespace hexit
{
    template<class T, class E>
    class Result {
    public:
        Result() : mIsValid(false) {}

        Result(Result<T, E> &&result)
            : mIsValid(result.mIsValid)
        {
            if(mIsValid) {
                new (&mOk) T(std::move(result.mOk));
            } else {
                new (&mErr) E(std::move(result.mErr));
            }
        }

        Result(T&& t)
            : mIsValid(true), mOk(std::forward<T>(t)) {}

        Result(E&& e)
            : mIsValid(false), mErr(std::forward<E>(e)) {}

        ~Result()
        {
            if(mIsValid) {
                mOk.~T();
            } else {
                mErr.~E();
            }
        }

        operator bool() const noexcept
        {
            return mIsValid;
        }

        T operator*() const noexcept
        {
            return mOk;
        }

        E Err() const noexcept
        {
            return mErr;
        }

    private:
        bool mIsValid;
        union
        {
            T mOk;
            E mErr;
        };
    };
}
