#include "game.hpp"

#include "playertype.hpp"
#include "states/gameplay.hpp"
#include "misc.hpp"

#include <SFML/Window/Event.hpp>

namespace
{
    const int kBoardSize = 11;
}

namespace hexit
{
    Game::Game()
        : mWindow(sf::VideoMode(800, 600), "HEXIT", sf::Style::Close)
    {
        mContext.font.loadFromFile("data/fonts/forward.ttf");

        mContext.robotThinkingTime = std::chrono::milliseconds(1000);
        mContext.colorSet = { sf::Color::Blue, sf::Color::Red };
        mContext.players = { PlayerType::Bot, PlayerType::Human };

        mContext.message = sf::Text("", mContext.font);

        handleTransition(
            states::Transition::push(
                std::unique_ptr<states::IState>(
                    new states::GamePlay(&mContext, kBoardSize))));
    }

    void Game::run()
    {
        mLastUpdate = std::chrono::steady_clock::now();
        while(mWindow.isOpen()) {
            processEvents();
            update();
            draw();
        }
    }

    void Game::processEvents()
    {
        sf::Event event;
        while(mWindow.pollEvent(event)) {
            if(mStates.empty()) {
                mWindow.close();
            }
            if(event.type == sf::Event::Closed) {
                mWindow.close();
            } else {
                handleTransition(mStates.top()->handleEvent(event));
            }
        }
    }

    void Game::update()
    {
        using namespace std::chrono;
        const steady_clock::time_point now = steady_clock::now();
        const milliseconds delta = duration_cast<milliseconds>(now - mLastUpdate);
        if(!mStates.empty()) {
            handleTransition(mStates.top()->update(delta));
        }
        mLastUpdate = now;
    }

    void Game::draw()
    {
        mWindow.clear();

        sf::Transform transform;
        transform.translate(sf::Vector2f(mWindow.getSize() / 2u));
        mWindow.draw(*mContext.boardView, transform);

        mWindow.draw(mContext.message, transform);

        mWindow.display();
    }

    void Game::handleTransition(states::Transition transition)
    {
        switch(transition.type()) {
        case states::TransitionType::Quit:
            while(!mStates.empty()) {
                mStates.top()->finished();
                mStates.pop();
            }
            break;

        case states::TransitionType::Push:
            if(!mStates.empty())
            {
                mStates.top()->hidden();
            }
            mStates.push(transition.grabState());
            mStates.top()->started();
            mStates.top()->exposed();
            break;

        case states::TransitionType::Pop:
            if(!mStates.empty())
            {
                mStates.top()->finished();
                mStates.pop();
                if(!mStates.empty())
                {
                    mStates.top()->exposed();
                }
            }
            break;

        case states::TransitionType::Replace:
            if(!mStates.empty()) {
                mStates.top()->finished();
                mStates.pop();
            }
            mStates.push(transition.grabState());
            mStates.top()->started();
            mStates.top()->exposed();
            break;

        case states::TransitionType::None:
            break;
        }
    }
}
