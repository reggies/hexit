#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include <vector>
#include <algorithm>

#include <SFML/System/Vector2.hpp>

template<class T>
sf::Rect<T> getBoundingRect(const std::vector<sf::Vector2<T>> &points)
{
    if(!points.empty()) {
        T top = points.at(0).y;
        T left = points.at(0).x;
        T bottom = top;
        T right = left;
            
        for(const sf::Vector2<T> &point : points) {
            top = std::min(top, point.y);
            left = std::min(left, point.x);
            bottom = std::max(bottom, point.y);
            right = std::max(right, point.x);
        }

        return sf::Rect<T>(top, left, right - left, bottom - top);
    } else {
        return sf::Rect<T>();
    }
}

template<class T>
sf::Rect<T> getCombinedRect(const std::vector<sf::Rect<T>> &rects)
{
    std::vector<sf::Vector2<T>> points;
    for(const sf::Rect<T> &rect : rects) {
        points.emplace_back(rect.left, rect.top);
        points.emplace_back(rect.left + rect.width, rect.top + rect.height);
    }
    return getBoundingRect(points);
}

#endif // GEOMETRY_H_
