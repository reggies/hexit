#ifndef CELLMAP_H_
#define CELLMAP_H_

#include <unordered_map>

#include "cellid.hpp"

namespace hexit
{
    struct CellIdHash
    {
        inline std::size_t operator()(const CellId &cell) const;
    };

    inline std::size_t CellIdHash::operator()(const CellId &cell) const
    {
        // It is two-dimensional array represented by hash table
        return (cell.y << 16) ^ (cell.x);
    }
    
    template<class T>
    using CellMap = std::unordered_map<CellId, T, CellIdHash>;

    template<class T>
    const T& getOrElse(const CellMap<T> &map, const CellId &id, const T &notFound)
    {
        const auto found = map.find(id);
        if(found == map.end()) {
            return notFound;
        } else {
            return found->second;
        }
    }
}

#endif // CELLMAP_H_
