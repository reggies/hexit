#ifndef CELLSTATE_H_
#define CELLSTATE_H_

#include <memory>
#include <chrono>

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include "cellid.hpp"
#include "anim/animation.hpp"

namespace hexit
{
    /**
     * \brief Cell's appearance state.
     *
     * It determines how the cell being drawn.
     */
    class CellState
    {
    public:
        CellState();
        virtual ~CellState() {}

        virtual void update(const std::chrono::milliseconds &elapsed);
        virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const {};
    protected:
        std::chrono::milliseconds getElapsedTime() const;

    private:
        std::chrono::milliseconds mElapsed;
    };
}

namespace hexit
{
    class SelectionCellState : public CellState
    {
    public:
        explicit SelectionCellState(float cellRadius, const sf::Color &color);

        ~SelectionCellState() override {}

        void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    private:
        float mCellRadius;
        sf::Color mColor;
    };
}

namespace hexit
{
    class BoardCellState : public CellState
    {
    public:
        explicit BoardCellState(float cellRadius, const sf::Color &fill, const sf::Color &outline);
        ~BoardCellState() override {}

        void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    private:
        float mCellRadius;
        sf::Color mFillColor;
        sf::Color mOutlineColor;
    };
}

namespace hexit
{
    class TransitionCellState : public CellState
    {
    public:
        explicit TransitionCellState(float cellRadius, const sf::Color &init, const sf::Color &last, const sf::Color &outline);

        ~TransitionCellState() override {}

        void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

    private:
        float mCellRadius;
        sf::Color mInitColor;
        sf::Color mLastColor;
        sf::Color mOutlineColor;
    };
}

#endif // CELLSTATE_H_
