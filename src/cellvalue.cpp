#include "cellvalue.hpp"

namespace hexit
{
    CellValue::CellValue()
        : mEmpty(true)
        , mPlayer(0)
    {
    }

    CellValue::CellValue(unsigned player)
        : mEmpty(false)
        , mPlayer(player)
    {
    }

    bool CellValue::empty() const
    {
        return mEmpty;
    }

    unsigned CellValue::player() const
    {
        return mPlayer;
    }
    
    bool CellValue::operator==(const CellValue &that) const
    {
        return (mEmpty == that.mEmpty) && (mPlayer == that.mPlayer);
    }

    bool CellValue::operator!=(const CellValue &that) const
    {
        return !this->operator==(that);
    }
}
