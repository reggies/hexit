#include "board.hpp"

#include <stdexcept>

#include "misc.hpp"
#include "bfs.hpp"

namespace
{
    const unsigned int kNumPlayerCount = 2;
    const unsigned int kPieTurnNumber = 1;
}

namespace hexit
{
    Board::Board(int size)
        : mSize(size)
        , mTurnNumber(0)
        , mCurrentPlayerId(0)
    {
    }

    int Board::size() const
    {
        return mSize;
    }

    unsigned Board::getCurrentPlayerId() const
    {
        return mCurrentPlayerId;
    }

    unsigned Board::getOtherPlayerId(unsigned player) const
    {
        return modulo(player + 1, kNumPlayerCount);
    }

    bool Board::isPieAvailable() const
    {
        return mTurnNumber == kPieTurnNumber;
    }

    void Board::beginNextTurn()
    {
        mTurnNumber += 1;
        mCurrentPlayerId = getOtherPlayerId(mCurrentPlayerId);
    }

    bool Board::getWinnerPlayerId(unsigned *winner) const
    {
        for(unsigned id = 0; id < kNumPlayerCount; ++id) {
            const std::vector<CellId> &chain = findCompleteChain(id);
            if(!chain.empty()) {
                *winner = id;
                return true;
            }
        }
        return false;
    }

    std::pair<BoardSide, BoardSide> getPlayerSides(unsigned player)
    {
        if(player == 0) {
            return {BoardSide::Top, BoardSide::Bottom};
        } else {
            return {BoardSide::Left, BoardSide::Right};
        }
    }

    unsigned Board::getSidePlayer(BoardSide side) const
    {
        switch(side) {
        case BoardSide::Top:
        case BoardSide::Bottom:
            return 0;

        case BoardSide::Left:
        case BoardSide::Right:
            return 1;

        default:
            throw std::runtime_error("bad side");
        }
    }

    std::vector<CellId> getCellsLine(int x, int y, int dx, int dy, int count)
    {
        std::vector<CellId> temp(count);
        for(int i = 0; i < count; ++i) {
            temp[i].x = x + dx * i;
            temp[i].y = y + dy * i;
        }
        return temp;
    }

    std::vector<CellId> Board::getSideCells(BoardSide side) const
    {
        switch(side) {
        case BoardSide::Top:
            return getCellsLine(0, 0, 1, 0, mSize);
        case BoardSide::Bottom:
            return getCellsLine(0, mSize-1, 1, 0, mSize);
        case BoardSide::Left:
            return getCellsLine(0, 0, 0, 1, mSize);
        case BoardSide::Right:
            return getCellsLine(mSize-1, 0, 0, 1, mSize);
        default:
            throw std::runtime_error("bad side");
        }
    }

    std::vector<CellId> Board::findCompleteChain(unsigned player) const
    {
        const std::pair<BoardSide, BoardSide> &sides = getPlayerSides(player);

        BreadthFirstTree tree(*this);

        for(const CellId &u : getSideCells(sides.first)) {
            if(getCell(u) == CellValue(player)) {
                tree.addVertex(u);
            }
        }

        CellId chainEnd;
        bool chainFound = false;
        for(const CellId &v : getSideCells(sides.second)) {
            if(tree.wasDiscovered(v)) {
                chainFound = true;
                chainEnd = v;
                break;
            }
        }

        if(chainFound) {
            return tree.getPath(chainEnd);
        } else {
            return {};
        }
    }

    bool Board::hasCell(const CellId &cell) const
    {
        return ((cell.x >= 0) && (cell.x < mSize))
            && ((cell.y >= 0) && (cell.y < mSize));
    }

    void Board::setCell(const CellId &cell, const CellValue &value)
    {
        mCells[cell] = value;
    }

    CellValue Board::getCell(const CellId &cell) const
    {
        return getOrElse(mCells, cell, CellValue());
    }

    std::vector<CellId> Board::getCells() const
    {
        std::vector<CellId> temp;
        temp.reserve(mSize * mSize);
        for(int x = 0; x < mSize; ++x) {
            for(int y = 0; y < mSize; ++y) {
                temp.emplace_back(x, y);
            }
        }
        return temp;
    }

    std::vector<CellId> Board::collectEmptyCells() const
    {
        std::vector<CellId> result;
        for(const CellId &id : getCells()) {
            if(getCell(id).empty()) {
                result.push_back(id);
            }
        }
        return result;
    }

    std::vector<BoardMove> Board::collectValidMoves() const
    {
        std::vector<BoardMove> moves;

        if(isPieAvailable()) {
            moves.push_back(BoardMove(MoveType::Pie));
        }

        for(const CellId& id : collectEmptyCells()) {
            moves.push_back(BoardMove(MoveType::Cell, id));
        }

        return moves;
    }
}
