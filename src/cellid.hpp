#ifndef CELLID_H_
#define CELLID_H_

#include <iosfwd>
#include <vector>

namespace hexit
{
    /**
     * \brief Cell identifier.
     *
     * Determines cell on hexagonal grid with axial coordinates x and y.
     */
    struct CellId
    {
        CellId();
        CellId(int x, int y);
        
        int x;
        int y;

        inline bool operator<(const CellId &that) const {
            return x < that.x || (x == that.x && y < that.y);
        }
        
        bool operator==(const CellId &that) const;
        bool operator!=(const CellId &that) const;
    };
    
    std::ostream& operator<<(std::ostream &out, const CellId &cell);
}

#endif // CELLID_H_
