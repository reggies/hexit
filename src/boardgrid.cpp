#include "boardgrid.hpp"

#include <cmath>

namespace hexit
{
    sf::Vector2f evalCellGridPosition(const CellId &cell)
    {
        return sf::Vector2f(sqrt(3) * (cell.x + 0.5f * cell.y), 1.5f * cell.y);
    }

    sf::Vector2f BoardGrid::getCellPosition(const CellId &cell, float radius) const
    {
        return evalCellGridPosition(cell) * radius;
    }

    std::vector<CellId> cornerCells(const Board &board)
    {
        return {
            {-1, -1},
            {board.size(), -1},
            {board.size(), board.size()},
            {-1, board.size()}
        };
    }
    
    std::vector<sf::Vector2f> BoardGrid::getCorners(const Board &board, float radius) const
    {
        std::vector<sf::Vector2f> points;
        for(const CellId &id : cornerCells(board)) {
            points.push_back(getCellPosition(id, radius));
        }
        return points;
    }
}
