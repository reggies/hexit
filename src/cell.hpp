#ifndef CELL_H_
#define CELL_H_

#include <memory>
#include <chrono>

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include "cellid.hpp"
#include "cellstate.hpp"
#include "anim/animation.hpp"

namespace hexit
{
    /**
     * \brief An entity of BoardView's scene.
     */
    class Cell : public sf::Drawable, public sf::Transformable
    {
    public:
        explicit Cell(const CellId &cell = CellId());

        void update(const std::chrono::milliseconds &elapsed);

        void setId(const CellId &cell);
        CellId getId() const;
        
        void setDrawOrder(int order);
        int getDrawOrder() const;

        sf::Color getColor() const;
        void setColor(const sf::Color &color);
        
        template<class T, class ...Args>
        void setAnimation(Args&&... args) {
            mAnimation.reset(new T(args...));
        }

        template<class T, class ...Args>
        void setState(Args&&... args) {
            mState.reset(new T(args...));
        }
        
    private:
        CellId mCell;
        int mDrawOrder;
        std::unique_ptr<anim::Animation> mAnimation;
        std::unique_ptr<CellState> mState;

        void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    };
}

#endif // CELL_H_
