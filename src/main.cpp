#include "game.hpp"

int main(int argc, char *argv[])
{
    hexit::Game game;
    game.run();
    return 0;
}
