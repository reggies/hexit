#ifndef MISC_H_
#define MISC_H_

#include <cmath>
#include <chrono>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>

namespace
{
    template<class T, class Scalar>
    sf::Vector2<T> operator*(const sf::Vector2<T> &v, Scalar f)
    {
        return sf::Vector2<T>(v.x * f, v.y * f);
    }

    template<class T, class Scalar>
    sf::Rect<T> operator*(const sf::Rect<T> &rect, Scalar f)
    {
        return sf::Rect<T>(rect.left * f, rect.top * f, rect.width * f, rect.height * f);
    }
    
    template<class T, class M>
    T modulo(T a, M b)
    {
        return ((a % b) + b) % b;
    }

    template<class T>
    T degreesToRadians(T degrees)
    {
        return 2 * M_PI * (degrees % 360) / 360.f;
    }

    inline float normalizeTime(const std::chrono::milliseconds &time, int period)
    {
        return modulo(time.count(), period) / static_cast<float>(period);
    }

    template<class T>
    sf::Vector2<T> getRectCenter(const sf::Rect<T> &rect)
    {
        return {(rect.left + rect.width) / 2, (rect.top + rect.height) / 2};
    }
}

#endif // MISC_H_
