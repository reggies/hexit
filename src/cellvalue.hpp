#ifndef CELLVALUE_H_
#define CELLVALUE_H_

namespace hexit
{
    /**
     * \brief Board's cell value.
     */
    class CellValue
    {
    public:
        CellValue();
        explicit CellValue(unsigned player);

        bool empty() const;
        unsigned player() const;

        bool operator==(const CellValue &that) const;
        bool operator!=(const CellValue &that) const;
        
    private:
        bool mEmpty;
        unsigned mPlayer;
    };
}

#endif // CELLVALUE_H_
