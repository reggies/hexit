#include "hexagonshape.hpp"

namespace hexit
{
    HexagonShape::HexagonShape(float radius)
        : sf::CircleShape(radius, 6)
    {
    }
}
