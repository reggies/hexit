#ifndef BOARDVIEW_H_
#define BOARDVIEW_H_

#include <memory>
#include <chrono>

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/Color.hpp>

#include "cell.hpp"
#include "cellid.hpp"
#include "cellmap.hpp"
#include "borders.hpp"
#include "boardgrid.hpp"

#include "anim/animation.hpp"

namespace hexit
{
    class Board;
}

namespace hexit
{
    /**
     * \brief Drawable representation of Board
     */
    class BoardView : public sf::Drawable, public sf::Transformable
    {
    public:
        explicit BoardView(const Board &board, float cellRadius, const std::vector<sf::Color> &palette);

        void activateSelection(bool activated);
        CellId getSelectedCell() const;
        void setSelectedCell(const CellId &cell);
        void update(const std::chrono::milliseconds &delta);

        void updateCellValue(const CellId &id, const CellValue &value);
        void shakeSelection();
        void animateChain(const std::vector<CellId> &chain);
        void moveSelection(int down, int right);

        sf::FloatRect getLocalBounds() const;
        sf::FloatRect getGlobalBounds() const;

    private:
        const Board &mBoard;
        bool mShowSelection;
        std::vector<sf::Color> mColors;
        sf::Color mEmptyCellColor;
        sf::Color mCellOutlineColor;
        int mDrawOrder;
        Cell mSelection;
        float mCellRadius;
        BoardGrid mGrid;
        Borders mBorders;
        CellMap<Cell> mCells;

    private:
        void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

        std::vector<CellId> getOrderedCells() const;
        sf::Color getCellColor(const CellId &id) const;
        sf::Color getCellValueColor(const CellValue &value) const;
        sf::Color getPlayerColor(unsigned player) const;
        void bringToFront(Cell *cell);
    };
}

#endif // BOARDVIEW_H_
