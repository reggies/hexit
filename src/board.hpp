#ifndef BOARD_H_
#define BOARD_H_

#include <vector>

#include "cellid.hpp"
#include "cellvalue.hpp"
#include "cellmap.hpp"
#include "boardmove.hpp"

namespace hexit
{
    enum class BoardSide {Top, Left, Right, Bottom};

    class Board
    {
    public:
        explicit Board(int size);

        int size() const;
        unsigned getCurrentPlayerId() const;
        unsigned getOtherPlayerId(unsigned player) const;

        void beginNextTurn();
        bool isPieAvailable() const;

        bool getWinnerPlayerId(unsigned *player) const;
        std::vector<CellId> findCompleteChain(unsigned player) const;

        bool hasCell(const CellId &id) const;
        CellValue getCell(const CellId &id) const;
        void setCell(const CellId &id, const CellValue &value);

        std::vector<CellId> getCells() const;
        unsigned getSidePlayer(BoardSide side) const;
        std::vector<CellId> getSideCells(BoardSide side) const;

        std::vector<BoardMove> collectValidMoves() const;

        std::vector<CellId> collectEmptyCells() const;

    private:
        int mSize;
        unsigned mTurnNumber;
        unsigned mCurrentPlayerId;
        CellMap<CellValue> mCells;
    };
}

#endif // BOARD_H_
