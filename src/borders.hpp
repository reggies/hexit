#ifndef BORDERS_H_
#define BORDERS_H_

#include <vector>

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/Color.hpp>

namespace hexit
{
    class Board;
    class BoardGrid;
}

namespace hexit
{
    /**
     * \brief BoardView background decoration.
     */
    class Borders : public sf::Drawable
    {
    public:
        Borders(const Board &board, const BoardGrid &grid, float cellRadius, float width, const sf::Color &bg, const std::vector<sf::Color> &palette);

        sf::FloatRect getBounds() const;
        
    private:
        const Board &mBoard;
        const BoardGrid &mGrid;
        std::vector<sf::ConvexShape> mBorders;
        float mCellRadius;
        float mBorderWidth;
        std::vector<sf::Color> mColors;
        
    private:
        void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    };
}

#endif // BORDERS_H_
