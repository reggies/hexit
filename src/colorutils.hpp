#ifndef COLORUTILS_H_
#define COLORUTILS_H_

#include <numeric>
#include <vector>

#include <SFML/Graphics/Color.hpp>

namespace 
{
    template<class Scalar>
    const sf::Color operator*(const sf::Color &color, Scalar f)
    {
        return sf::Color(color.r * f, color.g * f, color.b * f, color.a * f);
    }
    
    const sf::Color mixColors(const sf::Color &lhs, const sf::Color &rhs, float ratio)
    {
        return rhs * ratio + lhs * (1.f - ratio);
    }
    
    const sf::Color blendColors(const sf::Color &lhs, const sf::Color &rhs)
    {
        return mixColors(lhs, rhs, 0.5f);
    }

    const sf::Color getDarkenColor(const sf::Color &color)
    {
        return mixColors(sf::Color::Black, color, 0.3f);
    }

    const sf::Color getLightenColor(const sf::Color &color)
    {
        return mixColors(sf::Color::White, color, 0.3f);
    }
    
    const sf::Color foldBlendColors(const std::vector<sf::Color> &colors)
    {
        if(!colors.empty()) {
            return std::accumulate(colors.begin(), colors.end(), colors.front(), &blendColors);
        }
        return sf::Color();
    }
}

#endif // COLORUTILS_H_
