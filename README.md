# Overview

HEXIT is hex board game. It has been written in C++ and SFML.

# Requirements

* sfml >= 2.0

# How to build

```
mkdir build
cd build
cmake ..
cd ..
make -C build
```

# How to play

Key     | Action
---------:| :-----
*Arrows*| Move cursor
Return| Put a stone
Space | Switch sides(second move)

![screenshot][screenshot1]

[screenshot1]: media/screenshot.png "Screenshot"
